package com.kelompok1.tukangtaman.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.kelompok1.tukangtaman.core.TamanAsri;
import com.kelompok1.tukangtaman.core.TamanRumahmu;
import com.kelompok1.tukangtaman.core.TamanSejukRindang;
import com.kelompok1.tukangtaman.core.TukangTaman;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class TukangTamanRepositoryTest {
    private TukangTamanRepository tukangTamanRepository;

    @Mock
    private List<TukangTaman> sampleTukangTamanList;

    @BeforeEach
    void setUp() {
        tukangTamanRepository = new TukangTamanRepository();
        sampleTukangTamanList = new ArrayList<>();
        sampleTukangTamanList.add(new TamanSejukRindang("Taman Sejuk", "Hubungi Line tmnsejuk"));
        sampleTukangTamanList.add(new TamanRumahmu("Taman Rumah", "Hubungi Line tmnrumah"));
    }

    @Test
    void testWhenGetTukangTamanListMethodIsCalledItShouldReturnTukangTamanList() {
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        List<TukangTaman> listTukangTaman = tukangTamanRepository.getListTukangTaman();
        assertEquals(listTukangTaman, sampleTukangTamanList);
    }

    @Test
    void testWhenAddTukangTamanMethodIsCalledItShouldAddTukangTamanToList() {
        int sizeTukangTamanListLama = sampleTukangTamanList.size();
        ReflectionTestUtils.setField(tukangTamanRepository,
                "listTukangTaman", sampleTukangTamanList);
        tukangTamanRepository.addTukangTaman(
                new TamanAsri("Taman Asri", "Hubungi Line tmnasri"));
        int sizeTukangTamanListBaru = sampleTukangTamanList.size();
        assertEquals(sizeTukangTamanListLama + 1, sizeTukangTamanListBaru);
    }
}
