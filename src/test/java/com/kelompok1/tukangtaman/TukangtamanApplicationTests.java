package com.kelompok1.tukangtaman;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TukangtamanApplicationTests {
	@Test
	void contextLoads() {}

	@Test
	void applicationContextTest() {
		TukangtamanApplication.main(new String[] {});
	}
}
