package com.kelompok1.tukangtaman.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.kelompok1.tukangtaman.core.*;
import com.kelompok1.tukangtaman.repository.TukangTamanRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TukangTamanServiceImplTest {
    @Mock
    private TukangTamanRepository tukangTamanRepository;

    @InjectMocks
    private TukangTamanServiceImpl tukangTamanService;

    private List<TukangTaman> tukangtamans;

    @BeforeEach
    void setUp() {
        tukangtamans = new ArrayList<>();
        tukangtamans.add(new TamanAsri("Taman Asri", "deskripsi"));
        tukangtamans.add(new TamanHijauDaun("Taman Hijau", "deskripsi"));
        tukangtamans.add(new TamanJayaAgung("Taman Jaya", "deskripsi"));
        tukangtamans.add(new TamanRumahmu("Taman Rumahmu", "deskripsi"));
        tukangtamans.add(new TamanSejukRindang("Taman Sejuk", "deskripsi"));
    }

    @Test
    void testWhenGetListTukangTamanMethodIsCalledItShouldReturnListTukangTaman() {
        when(tukangTamanRepository.getListTukangTaman()).thenReturn(tukangtamans);
        List<TukangTaman> tukangTamanList = tukangTamanService.getListTukangTaman();
        verify(tukangTamanRepository, times(1)).getListTukangTaman();
        assertEquals(tukangTamanList.size(), 5);
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanAsri() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Asri");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanAsri.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanHijauDaun() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Hijau Daun");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanHijauDaun.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanJayaAgung() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Jaya Agung");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanJayaAgung.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanRumahmu() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Rumahmu");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanRumahmu.class));
    }

    @Test
    void testAddTukangTamanWhichKategoriIsTamanSejukRindang() {
        tukangTamanService.addTukangTaman("Tukang Taman",
                "Hubungi WA 08112222333",
                "Taman Sejuk Rindang");
        verify(tukangTamanRepository, atLeastOnce()).addTukangTaman(any(TamanSejukRindang.class));
    }
}
