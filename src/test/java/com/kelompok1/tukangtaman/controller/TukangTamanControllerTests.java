package com.kelompok1.tukangtaman.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kelompok1.tukangtaman.core.*;
import com.kelompok1.tukangtaman.service.TukangTamanService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = TukangTamanController.class)
public class TukangTamanControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TukangTamanService tukangTamanService;

    private List<TukangTaman> generateTukangTamans() {
        List<TukangTaman> tukangtamans = new ArrayList<>();
        tukangtamans.add(new TamanAsri("Taman Asri", "deskripsi"));
        tukangtamans.add(new TamanHijauDaun("Taman Hijau", "deskripsi"));
        tukangtamans.add(new TamanJayaAgung("Taman Jaya", "deskripsi"));
        tukangtamans.add(new TamanRumahmu("Taman Rumahmu", "deskripsi"));
        tukangtamans.add(new TamanSejukRindang("Taman Sejuk", "deskripsi"));

        return tukangtamans;
    }

    private List<Kategori> generateKategoris() {
        List<Kategori> kategoris =
                new ArrayList<>();
        kategoris.add(new TukangTamanKolam());
        kategoris.add(new TukangTamanMinimalis());
        kategoris.add(new TukangTamanLandscape());

        return kategoris;
    }

    @Test
    public void testUrlTukangTaman() throws Exception {
        List<TukangTaman> tukangtamans = generateTukangTamans();
        when(tukangTamanService.getListTukangTaman()).thenReturn(tukangtamans);

        mockMvc.perform(get("/"))

                .andExpect(status().isOk())
                .andExpect(model().attribute("listtukangtaman", hasSize(5)));
    }

    @Test
    void testAddTukangTamanUrl() throws Exception {
        mockMvc.perform(get("/add-tukang-taman"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addTukangTaman"))
                .andExpect(model().attributeExists("kategoriTaman"))
                .andExpect(model().attribute("kategoriTaman", hasSize(5)))
                .andExpect(view().name("add-tukang-taman"));
    }

    @Test
    void testPostAddTukangTamanUrl() throws Exception {
        mockMvc.perform(post("/add-tukang-taman")
                .param("namaTukangTaman", "Hijau Asri")
                .param("deskripsiTukangTaman", "Hubungi WA 081122223333")
                .param("kategoriTukangTaman", "Taman Asri"))
                .andExpect(handler().methodName("postAddTukangTaman"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));
        verify(tukangTamanService, times(1))
                .addTukangTaman("Hijau Asri", "Hubungi WA 081122223333", "Taman Asri");
    }

}
