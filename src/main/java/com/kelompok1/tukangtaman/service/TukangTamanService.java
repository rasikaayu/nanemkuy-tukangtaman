package com.kelompok1.tukangtaman.service;

import com.kelompok1.tukangtaman.core.TukangTaman;
import com.kelompok1.tukangtaman.core.Review;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface TukangTamanService {
    List<TukangTaman> getListTukangTaman();

    TukangTaman getTukangTaman(String nama);
    String addReviewToTukangTaman(String namaTukangTaman, String nama, String Komentar);

    void addTukangTaman(String namaTukangTaman, String deskripsiTukangTaman,
                        String kategoriTukangTaman);
}
