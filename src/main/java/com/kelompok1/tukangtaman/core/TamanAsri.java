package com.kelompok1.tukangtaman.core;

public class TamanAsri extends TukangTaman {

    public TamanAsri(String nama, String deskripsi) {
        super(nama, deskripsi);
        this.setKategori(new TukangTamanMinimalis());
    }
}