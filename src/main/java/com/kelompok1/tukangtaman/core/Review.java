package com.kelompok1.tukangtaman.core;

public class Review {

    private String nama;
    private String komentar;

    public Review(String nama, String komentar) {
        this.nama = nama;
        this.komentar = komentar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }
}