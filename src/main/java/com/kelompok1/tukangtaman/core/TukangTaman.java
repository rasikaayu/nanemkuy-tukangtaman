package com.kelompok1.tukangtaman.core;

import java.util.ArrayList;
import java.util.List;
import com.kelompok1.tukangtaman.core.Review;

public abstract class TukangTaman {

    private String nama;
    private Kategori kategori;
    private String deskripsi;
    private List<Review> listReview;

    public TukangTaman(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.listReview = new ArrayList<>();
    }

    public String getNama() {
        return this.nama;
    }

    public String getDeskripsi() {
        return this.deskripsi;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return this.kategori.getRincian();
    }

    public List<Review> getListReview(){
        return this.listReview;
    }

    public Review addReview(Review newReview){
        List<Review> currentList = this.listReview;
        currentList.add(newReview);
        return newReview;
    }

    public void deleteReview(Review review){
        List<Review> currentList = this.listReview;
        boolean isRemoved = currentList.remove(review);
    }





}