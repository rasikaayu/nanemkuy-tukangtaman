package com.kelompok1.tukangtaman.core;

public class TamanSejukRindang extends TukangTaman {

    public TamanSejukRindang(String nama, String deskripsi) {
        super(nama, deskripsi);
        this.setKategori(new TukangTamanKolam());
    }
}