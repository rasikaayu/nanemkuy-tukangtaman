package com.kelompok1.tukangtaman.controller;

import com.kelompok1.tukangtaman.service.TukangTamanService;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import com.kelompok1.tukangtaman.service.TukangTamanService;

@Controller
public class TukangTamanController {

    @Autowired
    private TukangTamanService tukangTamanService;

    @GetMapping("/")
    public String tanamanHome(Model model) {
        model.addAttribute("listtukangtaman", tukangTamanService.getListTukangTaman());
        return "index";
    }

    @RequestMapping("/detail/{namaTukangTaman}")
    public String detailTukangTaman(@PathVariable("namaTukangTaman") String namaTukangTaman,
                                    Model model) {
        model.addAttribute("tukangtaman", tukangTamanService.getTukangTaman(namaTukangTaman));
        return "detailTukangTaman";
    }

    @PostMapping(value = "/detail/{namaTukangTaman}/add-review/")
    @ResponseBody
    public ResponseEntity<String> addReview(@RequestParam String nama,
                                            @RequestParam String komentar,
                                            @PathVariable(value = "namaTukangTaman") String namaTukangTaman) {
        String review = tukangTamanService.addReviewToTukangTaman(namaTukangTaman, nama, komentar);
        return ResponseEntity.ok(review);
    }

    /**
     * Controller untuk menampilakan form untuk membuat objek Tukang Taman.
     */
    @GetMapping("/add-tukang-taman")
    public String addTukangTaman(Model model) {
        List<String> kategoriTaman = Arrays.asList("Taman Asri", "Taman Hijau Daun",
                "Taman Jaya Agung", "Taman Rumahmu", "Taman Sejuk Rindang");
        model.addAttribute("kategoriTaman", kategoriTaman);
        return "add-tukang-taman";
    }

    /**
     * Controller untuk membuat dan menambahkan objek Tukang Taman.
     * @param namaTukangTaman Nama Tukang Taman
     * @param deskripsiTukangTaman Deskripsi Tukang Taman
     * @param kategoriTukangTaman Kategori Tukang Taman
     */
    @PostMapping("/add-tukang-taman")
    public String postAddTukangTaman(Model model, @RequestParam String namaTukangTaman,
                                     @RequestParam String deskripsiTukangTaman,
                                     @RequestParam String kategoriTukangTaman) {
        tukangTamanService.addTukangTaman(namaTukangTaman, deskripsiTukangTaman,
                kategoriTukangTaman);
        return "redirect:/";
    }
}
