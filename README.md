# Tugas Kelompok Advanced Programming - A1
# NanemKuy - TukangTaman
<br>

| Pipeline | Coverage |
| -------- | -------- |
| [![pipeline status](https://gitlab.com/rasikaayu/nanemkuy-tukangtaman/badges/master/pipeline.svg)](https://gitlab.com/rasikaayu/nanemkuy-tukangtaman/-/commits/master) | [![coverage report](https://gitlab.com/rasikaayu/nanemkuy-tukangtaman/badges/master/coverage.svg)](https://gitlab.com/rasikaayu/nanemkuy-tukangtaman/-/commits/master) |

### Anggota Kelompok:
- Achmad Fikri Adidharma - 1906398692
- Efrado Suryadi - 1906350995
- Raja Aldwyn James Napintor Sihombing - 1906308066
- Shafira Putri Novia Hartanti - 1906293316
- Rasika Ayuningtyas - 1706039673
